// networknews
// Copyright (C) 2023 mrkubax10 <mrkubax10@onet.pl>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import "fmt"

type Widget interface {
	Render()
	OnActivate()
	IsInteractive() bool
	IsLineBreak() bool
	OnHighlight()
	OnUnhighlight()
	GetWidth() uint
	GetHeight() uint
	GetX() uint
	GetY() uint
	SetX(x uint)
	SetY(y uint)
}

type LabelWidget struct {
	x uint
	y uint
	text string
}
func (self *LabelWidget) Render() {
	Terminal_setCursorPos(self.x, self.y)
	Terminal_setForegroundColor(TERMINAL_NORMAL_TEXT_COLOR)
	fmt.Print(self.text)
}
func (self *LabelWidget) OnActivate() {}
func (self *LabelWidget) IsInteractive() bool {
	return false
}
func (self *LabelWidget) IsLineBreak() bool {
	return false
}
func (self *LabelWidget) OnHighlight() {}
func (self *LabelWidget) OnUnhighlight() {}
func (self *LabelWidget) GetWidth() uint {
	return uint(len(self.text))
}
func (self *LabelWidget) GetHeight() uint {
	return 1
}
func (self *LabelWidget) GetX() uint {
	return self.x
}
func (self *LabelWidget) GetY() uint {
	return self.y
}
func (self *LabelWidget) SetX(x uint) {
	self.x = x
}
func (self *LabelWidget) SetY(y uint) {
	self.y = y
}

type ButtonWidget struct {
	x uint
	y uint
	caption string
	highlighted bool
	callback func(interface{})
	data interface{}
}
func (self *ButtonWidget) Render() {
	Terminal_setCursorPos(self.x, self.y)
	if self.highlighted {
		Terminal_setBackgroundColor(TERMINAL_ACTIVE_BUTTON_BACKGROUND_COLOR)
	} else {
		Terminal_setBackgroundColor(TERMINAL_BUTTON_BACKGROUND_COLOR)
	}
	Terminal_setForegroundColor(TERMINAL_BUTTON_TEXT_COLOR)
	fmt.Printf(" %s ", self.caption)
	Terminal_setBackgroundColor(TERMINAL_BACKGROUND_COLOR)
}
func (self *ButtonWidget) OnActivate() {
	self.callback(self.data)
}
func (self *ButtonWidget) IsInteractive() bool {
	return true
}
func (self *ButtonWidget) IsLineBreak() bool {
	return false
}
func (self *ButtonWidget) OnHighlight() {
	self.highlighted = true
}
func (self *ButtonWidget) OnUnhighlight() {
	self.highlighted = false
}
func (self *ButtonWidget) GetWidth() uint {
	return uint(len(self.caption))+2
}
func (self *ButtonWidget) GetHeight() uint {
	return 1
}
func (self *ButtonWidget) GetX() uint {
	return self.x
}
func (self *ButtonWidget) GetY() uint {
	return self.y
}
func (self *ButtonWidget) SetX(x uint) {
	self.x = x
}
func (self *ButtonWidget) SetY(y uint) {
	self.y = y
}

type LineBreakWidget struct {
	x uint
	y uint
}
func (self *LineBreakWidget) Render() {}
func (self *LineBreakWidget) OnActivate() {}
func (self *LineBreakWidget) IsInteractive() bool {
	return false
}
func (self *LineBreakWidget) IsLineBreak() bool {
	return true
}
func (self *LineBreakWidget) OnHighlight() {}
func (self *LineBreakWidget) OnUnhighlight() {}
func (self *LineBreakWidget) GetWidth() uint {
	return 0
}
func (self *LineBreakWidget) GetHeight() uint {
	return 1
}
func (self *LineBreakWidget) GetX() uint {
	return self.x
}
func (self *LineBreakWidget) GetY() uint {
	return self.y
}
func (self *LineBreakWidget) SetX(x uint) {
	self.x = x
}
func (self *LineBreakWidget) SetY(y uint) {
	self.y = y
}

var TUI_widgets []Widget
var TUI_running = true
var TUI_character byte
var TUI_currentActiveWidget uint = 0
func TUI_render() {
	for _, widget := range(TUI_widgets) {
		widget.Render()
	}
}
func TUI_update() {
	TUI_character = Terminal_readChar()
	var widgetCount = uint(len(TUI_widgets))
	if widgetCount==0 {
		return
	}
	if TUI_character=='\t' {
		var i uint = 0
		var changed = false
		for i = TUI_currentActiveWidget+1; i<widgetCount; i++ {
			if TUI_widgets[i].IsInteractive() {
				TUI_widgets[TUI_currentActiveWidget].OnUnhighlight()
				TUI_currentActiveWidget = i
				TUI_widgets[TUI_currentActiveWidget].OnHighlight()
				changed = true
				break
			}
		}
		if !changed {
			TUI_widgets[TUI_currentActiveWidget].OnUnhighlight()
			TUI_currentActiveWidget = 0
		}
	}
	if TUI_character==' ' || TUI_character=='\n' {
		TUI_widgets[TUI_currentActiveWidget].OnActivate()
	}
}
func TUI_positionWidget(widget Widget) {
	var width, _ = Terminal_getSize()
	var x = (width-widget.GetWidth())/2
	var y uint = 1
	var widgetCount = len(TUI_widgets)
	if widgetCount>0 {
		var lastRowWidth uint = 0
		var row = TUI_widgets[widgetCount-1].GetY()
		var rowWidgetIndex = 0
		if !TUI_widgets[widgetCount-1].IsLineBreak() {
			for i:=widgetCount-1; i>=0; i-- {
				if TUI_widgets[i].GetY()!=row {
					rowWidgetIndex = i+1
					break
				}
				lastRowWidth+=TUI_widgets[i].GetWidth()+1
			}
		}
		if lastRowWidth+widget.GetWidth()<=width && !TUI_widgets[widgetCount-1].IsLineBreak() {
			x = (width-lastRowWidth-widget.GetWidth())/2+lastRowWidth
			y = row
			var rowOffset uint = 0
			for i:=rowWidgetIndex; i<widgetCount; i++ {
				TUI_widgets[i].SetX((width-lastRowWidth-widget.GetWidth())/2+rowOffset)
				rowOffset+=TUI_widgets[i].GetWidth()
			}
		} else {
			y = row+1
		}
	}
	widget.SetX(x)
	widget.SetY(y)
}
func TUI_addWidget(widget Widget) {
	TUI_positionWidget(widget)
	TUI_widgets = append(TUI_widgets, widget)
}
