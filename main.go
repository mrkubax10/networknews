// networknews
// Copyright (C) 2023 mrkubax10 <mrkubax10@onet.pl>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import "fmt"
import "os"

func main() {
	if len(os.Args)!=2 {
		fmt.Printf("Usage: %s <nntp server>\n", os.Args[0])
		return
	}
	var argServer string = os.Args[1]
	server, err := Server_new(argServer, 119)
	if err!=nil {
		fmt.Printf("Failed to connect to NNTP server %s: %s\n", argServer, err.Error())
		return
	}

	Terminal_setKeyMode()
	GCurrentScreen = GroupSelectionScreen{&server}
	GCurrentScreen.Begin()
	for TUI_running {
		GCurrentScreen.Render()
		TUI_render()
		TUI_update()
		if TUI_character=='q' {
			TUI_running = false
		}
	}

	Terminal_reset()
	server.Delete()
}
