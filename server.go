// networknews
// Copyright (C) 2023 mrkubax10 <mrkubax10@onet.pl>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import "errors"
import "fmt"
import "net"
import "strconv"
import "strings"

type Server struct {
	connection *net.TCPConn
}

func Server_new(host string, port uint16) (Server, error) {
	var server Server
	address, err := net.ResolveTCPAddr("tcp", host+":"+strconv.Itoa(int(port)))
	if err!=nil {
		return server, err
	}
	connection, err := net.DialTCP("tcp", nil, address)
	if err!=nil {
		return server, err
	}
	response, err := NNTPResponse_newFromStream(connection, false)
	if err!=nil {
		return server, err
	}
	if response.status<200 || response.status>299 {
		return server, errors.New("Unexpected status response while connecting")
	}
	fmt.Printf("Connection to %s:%d successful\n", host, port)
	fmt.Println(response.statusLine)
	server.connection = connection
	return server, nil
}

func (self *Server) Delete() {
	self.connection.Close()
}

func (self *Server) ListGroups() ([]Group, error) {
	var request = NNTPCommand_new("LIST")
	_, err := self.connection.Write(request.Construct())
	if err!=nil {
		return []Group{}, err
	}
	response, err := NNTPResponse_newFromStream(self.connection, true)
	if response.status<200 || response.status>299 {
		return []Group{}, errors.New(fmt.Sprintf("Server responded with %u %s", response.status, response.statusLine))
	}
	if err!=nil {
		return []Group{}, err
	}

	var currentString string
	var result []Group
	for _, c := range(response.text) {
		if c=='\n' {
			var fields = strings.Fields(currentString)
			if len(fields)!=4 {
				return result, errors.New(fmt.Sprintf("Invalid newsgroup entry: '%s'", currentString))
			}
			currentString = ""
			high, err := strconv.Atoi(fields[1])
			if err!=nil {
				return result, err
			}
			low, err := strconv.Atoi(fields[2])
			if err!=nil {
				return result, err
			}
			var group = Group {
				fields[0],
				uint(high),
				uint(low),
				fields[3]=="y",
			}
			result = append(result, group)
			continue
		}
		currentString+=string(c)
	}
	return result, nil
}
