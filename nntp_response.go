// networknews
// Copyright (C) 2023 mrkubax10 <mrkubax10@onet.pl>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import "errors"
import "net"
import "strconv"
import "unicode"

type NNTPResponse struct {
	status uint
	statusLine string
	arguments []string
	text string
}

type ResponseParserState uint8
const (
	RPARSER_STATUS ResponseParserState = iota
	RPARSER_STATUS_LINE
	RPARSER_TEXT
)

func NNTPResponse_newFromStream(connection *net.TCPConn, withText bool) (NNTPResponse, error) {
	var result NNTPResponse
	var response string
	var received = make([]byte, 512)
	var newLine = true
	for {
		byteCount, err := connection.Read(received)
		if err!=nil {
			return result, err
		}
		if byteCount==0 {
			return result, errors.New("Invalid response: server didn't send any data")
		}
		response+=string(received[:byteCount])
		if byteCount>=2 && received[byteCount-2]=='\r' && received[byteCount-1]=='\n' {
			if withText {
				newLine = true
			} else {
				break
			}
		}
		if withText && newLine && byteCount>=3 && received[byteCount-3]=='.' && received[byteCount-2]=='\r' && received[byteCount-1]=='\n' {
			break
		}
	}

	var currentString string
	var state = RPARSER_STATUS
	var responseLength = len(response)
	newLine = true
	for i:=0; i<responseLength; i++ {
		switch state {
		case RPARSER_STATUS:
			if unicode.IsSpace(rune(response[i])) {
				if len(currentString)==0 {
					return result, errors.New("Invalid response: status code expected")
				}
				converted, err := strconv.Atoi(currentString)
				if err!=nil {
					return result, errors.New("Invalid response: "+err.Error())
				}
				result.status = uint(converted)
				currentString = ""
				state = RPARSER_STATUS_LINE
				continue
			}
		case RPARSER_STATUS_LINE:
			if response[i]=='\r' {
				if i==responseLength-1 {
					return result, errors.New("Invalid response: Unexpected end of input")
				}
				if response[i+1]!='\n' {
					return result, errors.New("Invalid response: Expected LF after CR")
				}
				result.statusLine = currentString
				if i+2<responseLength {
					currentString = ""
					state = RPARSER_TEXT
					i++
					continue
				}
				return result, nil
			}
		case RPARSER_TEXT:
			if i+1<responseLength {
				if newLine && response[i]=='.' {
					if response[i+1]=='\r' && i+2<responseLength && response[i+2]=='\n' {
						result.text = currentString
						return result, nil
					}
					if response[i+1]=='.' {
						continue
					}
				}
				if response[i+1]=='\r' && i+2<responseLength && response[i+2]=='\n' {
					newLine = true
					currentString+=string(response[i])
					currentString+="\n"
					i+=2
					continue
				}
			}
			newLine = false
		}
		currentString+=string(response[i])
	}
	return result, errors.New("Invalid response")
}
