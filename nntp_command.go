// networknews
// Copyright (C) 2023 mrkubax10 <mrkubax10@onet.pl>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

type NNTPCommand struct {
	arguments []string
}

func NNTPCommand_new(command string) NNTPCommand {
	var cmd NNTPCommand
	cmd.AddArgument(command)
	return cmd
}

func (self *NNTPCommand) AddArgument(argument string) {
	self.arguments = append(self.arguments, argument)
}

func (self *NNTPCommand) Construct() []byte {
	var result string
	var argumentCount = len(self.arguments)
	for i, element := range(self.arguments) {
		result+=element
		if i<argumentCount-1 {
			result+=" "
		}
	}
	result+="\r\n"
	return []byte(result)
}
