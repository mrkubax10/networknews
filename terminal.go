// networknews
// Copyright (C) 2023 mrkubax10 <mrkubax10@onet.pl>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import "fmt"
import "os"
// #include <sys/ioctl.h>
// #include <termios.h>
// typedef struct winsize winsize;
// typedef struct termios termios;
// void ioctl_wrapper(int i, unsigned long l, winsize* t) { ioctl(i, l, t); }
import "C"

var Terminal_prevTermiosMode C.termios

type TerminalColor uint8
const (
	TERMINAL_COLOR_GREEN TerminalColor = 32
	TERMINAL_COLOR_YELLOW TerminalColor = 33
	TERMINAL_COLOR_BLUE TerminalColor = 34
	TERMINAL_COLOR_WHITE TerminalColor = 37
	TERMINAL_COLOR_BRIGHT_WHITE TerminalColor = 97
)
const (
	TERMINAL_BACKGROUND_COLOR TerminalColor = TERMINAL_COLOR_BLUE
	TERMINAL_BUTTON_BACKGROUND_COLOR TerminalColor = TERMINAL_COLOR_GREEN
	TERMINAL_ACTIVE_BUTTON_BACKGROUND_COLOR TerminalColor = TERMINAL_COLOR_YELLOW
	TERMINAL_NORMAL_TEXT_COLOR TerminalColor = TERMINAL_COLOR_WHITE
	TERMINAL_BUTTON_TEXT_COLOR TerminalColor = TERMINAL_COLOR_BRIGHT_WHITE
)

func Terminal_setCursorPos(x uint, y uint) {
	fmt.Printf("\033[%d;%dH", y, x)
}

func Terminal_getSize() (uint, uint) {
	// TODO: Make this function more portable
	var terminalSize C.winsize
	C.ioctl_wrapper(0, 0x5413, &terminalSize)
	return uint(terminalSize.ws_col), uint(terminalSize.ws_row)
}

func Terminal_setBackgroundColor(bg TerminalColor) {
	fmt.Printf("\033[%dm", bg+10)
}

func Terminal_setForegroundColor(fg TerminalColor) {
	fmt.Printf("\033[%dm", fg)
}

func Terminal_resetFormatting() {
	fmt.Print("\033[0m")
}

func Terminal_clear() {
	fmt.Print("\033[2J")
}

func Terminal_clearWithColor(color TerminalColor) {
	Terminal_setCursorPos(1, 1)
	Terminal_setBackgroundColor(color)
	var width, height = Terminal_getSize()
	var i uint
	for i = 0; i<width*height; i++ {
		fmt.Print(" ")
	}
}

func Terminal_setKeyMode() {
	var newTermiosMode C.termios
	C.tcgetattr(0, &Terminal_prevTermiosMode)
	C.tcgetattr(0, &newTermiosMode)
	C.cfmakeraw(&newTermiosMode)
	C.tcsetattr(0, C.TCSANOW, &newTermiosMode)
}

func Terminal_resetMode() {
	C.tcsetattr(0, C.TCSANOW, &Terminal_prevTermiosMode)
}

func Terminal_readChar() byte {
	var output = make([]byte, 1)
	os.Stdin.Read(output)
	return output[0]
}

func Terminal_reset() {
	Terminal_resetMode()
	Terminal_resetFormatting()
	fmt.Print("\r")
	Terminal_clear()
}
