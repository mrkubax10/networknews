// networknews
// Copyright (C) 2023 mrkubax10 <mrkubax10@onet.pl>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import "fmt"

type GroupSelectionScreen struct {
	server *Server
}

func (self GroupSelectionScreen) Begin() {
	var groups, err = self.server.ListGroups()
	if err!=nil {
		fmt.Println(err.Error())
		return
	}
	var title = LabelWidget{0, 0, "Select newsgroup"}
	TUI_addWidget(&title)
	TUI_addWidget(&LineBreakWidget{0, 0})
	TUI_addWidget(&LineBreakWidget{0, 0})
	for _, group := range(groups) {
		var btn = ButtonWidget{0, 0, group.name, false, selectGroup, &group}
		TUI_addWidget(&btn)
		TUI_addWidget(&LineBreakWidget{0, 0})
	}
}

func (self GroupSelectionScreen) Render() {
	Terminal_clearWithColor(TERMINAL_BACKGROUND_COLOR)
}

func selectGroup(data interface{}) {
}
